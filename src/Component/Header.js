import React, { Component } from 'react'
class Header extends Component {
    constructor(props) {
        super(props)
        this.state = { date: new Date() }
    }
    tick() {
        this.setState({ date: new Date() })
    }
    componentDidMount() {
        this.timerID = setInterval(() => { this.tick() }, 1000)
    }
    componentDidUpdate() {   
    }
    componentWillUnmount() {
        clearInterval(this.timerID)
    }

    render() {
        return (
            <div className="container-fluid">
                <div className="row">
                    <div className="col-md-8 text-left">
                        <h2 className="title"> <img style={{ height: 150, width: 150 }} src="/images/logo.jpg" alt="" />Coffee Cafe</h2>
                    </div>
                    <div className="col-md-4 text-right">
                        <h5 className="text-muted mt-5">{this.state.date.toLocaleTimeString()}</h5>
                    </div>
                </div>

            </div>
        )
    }
}
export default Header