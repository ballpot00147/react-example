import React, { Component } from 'react'

class Calculator extends Component {
    showOrder (orders) {
        if(!orders || orders.length == 0){
            return <li className="text-right text-muted title">No have Product</li>
        }else{
            return orders.map(order => {
                return (
                <li className="text-right text-success title" key={order.product.ProductId}>
                      <br/>
                     {order.product.ProductName} x {order.quantity} = {order.product.UnitPrice * order.quantity} &nbsp;
                <button className="btn btn-light btn-sm" onClick={() => this.props.onDeleteOrder(order.product)}>Delete</button>
                 </li>
                )
            })
        }
    }
    render() {
        const {totalPrice,orders} = this.props
        return (
            <div>
                <h2 className="text-right">{totalPrice}</h2>
                <hr />
                <ul className="list-unstyled">      
                    {this.showOrder(orders)}
                </ul>
                <button className="btn btn-block btn-danger title">Confirm</button>
                <button className="btn btn-block btn-secondary title">Cancle</button>
            </div>
        )
    }
}
export default Calculator