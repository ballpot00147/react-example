import React, { Component } from 'react'

class ProductItem extends Component {
    constructor(props) {
        super(props)
        
    }
    // doSomething(ProductName){
    //     console.log(ProductName)
    // }
    render() {
        const { ProductName, UnitPrice,thumbnail } = this.props.products
        return (
            <div className="col-md-3 mt-2">
                <img style={{width:335,height:190}} className="img-fluid img-thumbnail" src={thumbnail} />
                <h5 className="mt-2">{ProductName}</h5>
                <p className="text-right">{UnitPrice} THB</p>
                <button  className="btn btn-secondary btn-block title" onClick={() => this.props.onAddOrder(this.props.products)}>Buy</button>
                <hr/>
            </div>
        )
    }
}
export default ProductItem