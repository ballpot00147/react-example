import React, { Component } from 'react'
import ProductItem from './ProductItem'
class ProductList extends Component {
    showProduct() {
        if (this.props.products) {
            return this.props.products.map(product => {
                // console.log(product.ProductName)e
                // return <ProductItem key={product.ProductId}  ProductName={product.ProductName} UnitPrice={product.UnitPrice} thumbnail={product.thumbnail} onAddOrder={this.props.onAddOrder} />
                return <ProductItem key={product.ProductId}  products={product} onAddOrder={this.props.onAddOrder} />
            })
            // return this.props.products.map(product => (
            // console.log(product.ProductName)e
            //      <ProductItem  ProductName={product.ProductName} UnitPrice={product.UnitPrice} thumbnail={product.thumbnail} />
            // ))
        }
    }
    componentDidMount() {
    }
    render() {
        return (
            <div className="row">
                {this.showProduct()}
            </div>
        )
    }
}
export default ProductList