import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import 'bootstrap/dist/css/bootstrap.min.css'
import reportWebVitals from './reportWebVitals';
// import configDeviceReducer from "./configDevice";
// const store = createStore(
//   allReducers,
//   compose(
//     applyMiddleware(
//       /* ----  middlewares ----  */
//       thunk
//     )

//     // window.__REDUX_DEVTOOLS_EXTENSION__&& window.__REDUX_DEVTOOLS_EXTENSION__(),
//   )
// );
ReactDOM.render(
 
  <React.StrictMode>
     {/* <Provider store={store}> */}
    <App />
    {/* </Provider> */}
  </React.StrictMode>,

  document.getElementById('root')
);
// const allReducers = combineReducers({
// configDevice: configDeviceReducer,
// })


reportWebVitals();
