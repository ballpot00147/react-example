import React,{Component} from 'react'
import Header from './Component/Header'
import Footer from './Component/Footer'
import Monitor from './Component/monitor/Monitor'
import './App.css'
class App extends Component {
  constructor(props){
    super(props)
    this.state = {products : ""}
  }
  componentDidMount(){
    this.setState({products:[
      { ProductId: 2, ProductName: "Coffee", UnitPrice:"130", thumbnail: "/images/coffee.jpg" },
      { ProductId: 3, ProductName: "Coffee", UnitPrice:"135", thumbnail: "/images/coffee.jpg" },
      { ProductId: 4, ProductName: "Coffee", UnitPrice:"140", thumbnail: "/images/coffee.jpg" },
      { ProductId: 5, ProductName: "Coffee", UnitPrice:"145", thumbnail: "/images/coffee.jpg" },
      { ProductId: 6, ProductName: "Coffee", UnitPrice:"150", thumbnail: "/images/coffee.jpg" },
    ]})
  }
  render(){
    return (
      <div className="App">
        <Header />
        <Monitor products={this.state.products}/>
        <Footer company="coffee" email="admin@gmail.com"/>
      </div>
    );
  }
}

// export default App;

// import React, { useState, useEffect } from 'react'

// const App = () => {
//   const [text, setText] = useState("")
//   const [time, setTime] = useState(0)

//   const handleSetText = () => {
//     let _text = text
//     _text = _text + "/" + _text
//     setText(_text)
//   }
  // function handleSetText()  {
  //   let _text = text
  //   _text = _text + "/" + _text
  //   setText(_text)
  // }

  // useEffect(() => {
  //   setText('hello world')
  // }, [])
  // [] เป็นค่าว่าง = componentDidMount
  // useEffect(() => {
  //   setTime(time + 1)
  // }, [text])
  // [value] มีค่า = componentDidUpdate

//   return (
//     <div>
//       {text}<br />
//       {time}<br />
//       <button onClick={() => {
//         handleSetText()
//       }}>Add</button>
//     </div>
//   )
// }
export default App;
